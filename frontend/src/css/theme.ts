import { createMuiTheme } from '@material-ui/core';

export default createMuiTheme({
  palette: {
    primary: {
      main: '#4D210E',
    },
    secondary: {
      main: '#B56C43',
    },
  },
  typography: {
    fontFamily: 'Open Sans',
  },
  overrides: {
    MuiButton: {
      text: {
        color: 'var(--text-darker)',
        fontWeight: 800,
        fontSize: '1.7em',
        textTransform: 'capitalize',
      },
      contained: {
        fontSize: '1em',
        color: 'white',
        fontWeight: 800,
        backgroundColor: 'var(--text-lighter)',
      },
    },
    MuiCard: {
      root: {
        borderRadius: '1.7em',
        boxShadow: 'var(--shadow)',
      },
    },
    MuiCardContent: {
      root: {
        fontFamily: 'Open Sans',
        padding: '1.5em',
        textAlign: 'center',
      },
    },
    MuiCardMedia: {
      root: {
        height: 250,
      },
    },
    MuiInput: {
      root: {
        color: 'var(--text-darker)',
        fontWeight: 700,
      },
    },
    MuiTableCell: {
      head: {
        fontSize: '1.1em',
        fontWeight: 800,
        textTransform: 'uppercase',
        color: 'var(--text-lighter)',
      },
      body: {
        fontWeight: 600,
        fontSize: '1em',
        color: 'var(--text-darker)',
      },
    },
    MuiFilledInput: {
      input: {
        borderRadius: 10,
        backgroundColor: 'var(--lighter-nav-color)',
        color: 'var(--text-darker)',
        fontSize: '1em',
        fontWeight: 800,
        position: 'relative',
        border: '4px solid var(--nav-color)',
        padding: '10px 26px 10px 12px',
        '&:focus': {
          borderRadius: '2em',
          borderColor: 'var(--text-lighter)',
          boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
      },
    },
  },
});
