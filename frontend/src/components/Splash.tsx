import React, { useState, useEffect } from 'react';
import styles from '../css/Splash.module.css';
import Logo from '../assets/logo.svg';
import Cat from '../assets/My_cat.png';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { RouteComponentProps } from 'react-router-dom';
import CallMadeIcon from '@material-ui/icons/CallMade';

const Splash: React.FunctionComponent<RouteComponentProps> = ({ location }) => {
  const [open, setOpen] = useState(false);
  useEffect(() => {
    setOpen(location.pathname === '/home');
  }, [location.pathname]);
  console.log(open);
  return (
    <div className={`${styles.bg} ${open ? '' : styles.closed}`}>
      <img
        className={`${styles.logo} ${open ? '' : styles.closed}`}
        src={Logo}
        alt="logo"
      />
      <div className={`${styles.title} ${open ? '' : styles.closed}`}>
        <h1 className={styles.darker}>Find the</h1>
        <h1 className={styles.darker}>Perfect Pet</h1>
        <h1 className={styles.lighter}>to adopt</h1>
      </div>
      <div className={`${styles.cta_bg} ${open ? '' : styles.closed}`}>
        <img className={styles.cat} src={Cat} alt="Cat" />
        <div className={styles.cta_text}>
          <p className={styles.first_line}>I want to adopt a </p>
          <div className={styles.second_line}>
            <Link to="/dogs">
              <Button
                endIcon={
                  <CallMadeIcon
                    style={{ fontSize: '1em', marginLeft: '-.2em' }}
                  />
                }
              >
                Dog
              </Button>
            </Link>
            <p className={styles.or}>or</p>
            <Link to="/cats">
              <Button
                endIcon={
                  <CallMadeIcon
                    style={{ fontSize: '1em', marginLeft: '-.2em' }}
                  />
                }
              >
                Cat
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Splash;
