import React from 'react';
import { NavLink } from 'react-router-dom';

const MobileNavBar = () => {
  return (
    <header>
      <nav
        className="navbar navbar-expand-lg navbar-dark"
        style={{
          backgroundColor: 'var(--text-lighter)',
        }}
      >
        <a className="navbar-brand" href="home.html">
          AdoptAPet
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li
              className="nav-item active"
              data-toggle="collapse"
              data-target=".navbar-collapse.show"
            >
              <NavLink to="/home" className="nav-link">
                Home
              </NavLink>
            </li>
            <li
              className="nav-item active"
              data-toggle="collapse"
              data-target=".navbar-collapse.show"
            >
              <NavLink to="/dogs" className="nav-link">
                Dog Breeds
              </NavLink>
            </li>
            <li
              className="nav-item active"
              data-toggle="collapse"
              data-target=".navbar-collapse.show"
            >
              <NavLink to="/cats" className="nav-link">
                Cat Breeds
              </NavLink>
            </li>
            <li
              className="nav-item active"
              data-toggle="collapse"
              data-target=".navbar-collapse.show"
            >
              <NavLink to="/adoption" className="nav-link">
                Adoption
              </NavLink>
            </li>
            <li
              className="nav-item active"
              data-toggle="collapse"
              data-target=".navbar-collapse.show"
            >
              <NavLink to="/about" className="nav-link">
                About
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default MobileNavBar;
