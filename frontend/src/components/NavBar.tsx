import React from 'react';
import styles from '../css/Navbar.module.css';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className={styles.bg}>
      <ul className={styles.list}>
        <li>
          <NavLink
            to="/home"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/dogs"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            Dog Breeds
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/cats"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            Cat Breeds
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/adoption"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            Adoption
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/about"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            About
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/group10"
            className={styles.nav_link}
            activeClassName={styles.active_nav_link}
          >
            Group10 API
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
