import React from 'react';
import ecstat from 'echarts-stat';
import ReactEcharts from 'echarts-for-react';

const PlayersHistogram = ({ data, title, x, y }: any) => {
  var bins = ecstat.histogram(data, 'squareRoot');
  var option = {
    title: {
      text: title,
      left: 'center',
      top: 20,
    },
    color: ['#4d210e'],
    grid: {
      containLabel: true,
    },
    xAxis: [
      {
        type: 'value',
        scale: true, //这个一定要设，不然barWidth和bins对应不上
        name: x,
      },
    ],
    yAxis: [
      {
        type: 'value',
        name: y,
      },
    ],
    series: [
      {
        name: 'height',
        type: 'bar',
        barWidth: '99.3%',
        label: {
          normal: {
            show: true,
            position: 'insideTop',
            formatter: function (params: any) {
              return params.value[1];
            },
          },
        },
        data: bins.data,
      },
    ],
  };
  return <ReactEcharts option={option} />;
};

export default PlayersHistogram;
