import React from 'react';
import '../css/main.css';
import styles from '../css/App.module.css';
import NavBar from '../components/NavBar';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import About from './About';
import Home from './Home';
import Dogs from './Dogs';
import Cats from './Cats';
import Adoption from './Adoption';
import Group10 from './Group10';
import Splash from '../components/Splash';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MobileNavBar from '../components/MobileNavBar';
import * as t from 'react-router-transition';

// function mapStyles(styles: {
//   opacity: number;
//   y: number;
// }): { opacity: number; transform: string } {
//   return {
//     opacity: styles.opacity,
//     transform: `transform-y(${styles.y})`,
//   };
// }

function App() {
  const isMobile = useMediaQuery('(max-width:600px)');
  return (
    <BrowserRouter>
      <div className={styles.bg}>
        {isMobile ? (
          <MobileNavBar />
        ) : (
          <>
            <Route path="/" component={Splash} />
            <Route path="/" component={NavBar} />
          </>
        )}
        <div className={styles.main}>
          <t.AnimatedSwitch
            atEnter={{ opacity: 0, marginTop: 300 }}
            atActive={{ opacity: 1, marginTop: 0 }}
            atLeave={{ opacity: 0, marginTop: -300 }}
            className={styles.transition}
          >
            <Route exact path="/">
              <Redirect to="/home" />
            </Route>
            <Route exact path="/home" component={Home}></Route>
            <Route exact path="/about" component={About}></Route>
            <Route exact path="/dogs" component={Dogs}></Route>
            <Route exact path="/cats" component={Cats}></Route>
            <Route exact path="/adoption" component={Adoption}></Route>
            <Route exact path="/group10" component={Group10}></Route>
          </t.AnimatedSwitch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
