import React, { useState, useEffect, useRef } from 'react';
import styles from '../css/MainPage.module.css';
import {
  Card,
  CardMedia,
  CardContent,
  CardActionArea,
  CardActions,
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';
import Axios from '../utils/API';
import SearchBar from '../components/SearchBar';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import Pagination from '@material-ui/lab/Pagination';
import Highlighter from 'react-highlight-words';
import { Helmet } from 'react-helmet';

const Dogs = () => {
  const [loading, setLoading] = useState(false);
  const [dogBreeds, setDogBreeds] = useState<any>([]);
  const [pagination, setPagination] = useState({ page: 0, count: 0 });
  const [page, setPage] = useState(1);
  const [sort, setSort] = useState('name');
  const [searchTerm, setSearchTerm] = useState('');

  const location = useLocation();

  useEffect(() => {
    setLoading(true);

    if (searchTerm) {
      Axios.get('/dog_breeds/search', {
        params: { search_for: searchTerm },
      }).then((res) => {
        setDogBreeds([...res.data.and_results, ...res.data.or_results]);
        setLoading(false);
      });
    } else {
      Axios.get(
        location.search
          ? '/dog_breeds/' + location.search.substr(1)
          : '/dog_breeds',
        {
          params: {
            page: location.search ? null : page,
            sort_by: location.search ? null : sort || null,
          },
        }
      ).then((res) => {
        setDogBreeds(res.data.dog_breeds);
        !location.search &&
          setPagination({
            page: res.data.meta_data.page,
            count: res.data.meta_data.total_pages,
          });
        setLoading(false);
      });
    }
  }, [page, location.search, sort, searchTerm]);

  return (
    <main>
      <Helmet>
        <title>Dog Breeds</title>
      </Helmet>
      <div className={styles.body}>
        <Loading show={loading} showSpinner={true} color="var(--text-darker)" />
        <div className={styles.top}>
          <h1 className={styles.title}>Dog Breeds</h1>
          <span className={styles.subtitle}>
            Which breed of dogs would you like to adopt?
          </span>
          {!location.search && (
            <SearchBar setParentSearchTerm={setSearchTerm} />
          )}
          <br />
          <br />
          {/* <FormControl variant="filled">
            <InputLabel id="sort">Sort by</InputLabel> */}
          {/* </FormControl> */}
          {location.search && (
            <Link to="/dogs">
              <Button variant="contained">show full list</Button>
            </Link>
          )}
          {!(location.search || searchTerm) && (
            <>
              <p style={{ marginBottom: '5px' }}>Sort by: </p>
              <Select
                labelId="sort"
                value={sort}
                onChange={(event) => setSort(event.target.value + '')}
                variant="filled"
                style={{ marginBottom: '2em' }}
              >
                <MenuItem value="">None</MenuItem>
                <MenuItem value={'name'}>Name</MenuItem>
                <MenuItem value={'temperament'}>Temperament</MenuItem>
                <MenuItem value={'life_span'}>Life span</MenuItem>
                <MenuItem value={'weight_imperial'}>Weight (imperial)</MenuItem>
                <MenuItem value={'weight_metric'}>Weight (metric)</MenuItem>
                <MenuItem value={'height_metric'}>Height</MenuItem>
              </Select>
              <Pagination
                color="primary"
                page={page}
                count={pagination.count}
                onChange={(event, value) => setPage(value)}
                boundaryCount={2}
              />
            </>
          )}
        </div>
        <div className={styles.cards_container}>
          {dogBreeds.map((dog: Dog, id: number) => (
            <Card key={id}>
              <CardMedia image={dog.image_url} />
              <CardContent>
                <span className={styles.card_title}>
                  <Highlighter
                    searchWords={searchTerm.split(' ')}
                    textToHighlight={dog.name}
                  />
                </span>
                <div className={styles.card_description}>
                  <div className={styles.row}>
                    <h3>Temperament: </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={dog.temperament}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Life span: </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={dog.life_span}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Weight (imperial): </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={dog.weight_imperial}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Weight (metric): </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={dog.weight_metric}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Height: </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={dog.height_imperial}
                    />
                  </div>
                </div>
              </CardContent>
              <CardActions
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  marginBottom: '2em',
                }}
              >
                <Link
                  to={{ pathname: '/adoption', search: dog.name }}
                  style={{
                    display: 'block',
                  }}
                >
                  <Button variant="contained">Find one to adopt</Button>
                </Link>
              </CardActions>
            </Card>
          ))}
        </div>
        {!(location.search || searchTerm) && (
          <Pagination
            color="primary"
            page={page}
            count={pagination.count}
            onChange={(event, value) => setPage(value)}
            boundaryCount={2}
          />
        )}
      </div>
    </main>
  );
};

interface Dog {
  height_imperial: string;
  height_metric: string;
  image_url: string;
  life_span: string;
  name: string;
  temperament: string;
  weight_imperial: string;
  weight_metric: string;
}

export default Dogs;
