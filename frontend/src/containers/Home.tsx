import React from 'react';
import bg from '../assets/splash.jpeg';
import styles from '../css/MainPage.module.css';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import CallMadeIcon from '@material-ui/icons/CallMade';
import { Helmet } from 'react-helmet';

const Home = () => {
  return (
    <div
      style={{
        backgroundImage: `url(${bg})`,
      }}
      className={styles.home_bg}
    >
      <Helmet>
        <title>Home</title>
      </Helmet>
      <p className={styles.photo_credit}>Photo by by Mikkel Bigandt</p>
      <div className={styles.splash}>
        <h1 className={`${styles.home_title}`}>Find the Perfect Pet</h1>
        <h1 className={`${styles.home_title_second}`}>to adopt</h1>
        <div className={`${styles.cta_bg}`}>
          <div className={styles.cta_text}>
            <p className={styles.first_line}>I want to adopt a </p>
            <div className={styles.second_line}>
              <Link to="/dogs">
                <Button
                  endIcon={
                    <CallMadeIcon
                      style={{ fontSize: '1em', marginLeft: '-.2em' }}
                    />
                  }
                >
                  Dog
                </Button>
              </Link>
              <p className={styles.or}>or</p>
              <Link to="/cats">
                <Button
                  endIcon={
                    <CallMadeIcon
                      style={{ fontSize: '1em', marginLeft: '-.2em' }}
                    />
                  }
                >
                  Cat
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
