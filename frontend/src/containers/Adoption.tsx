import React, { useState, useEffect, useRef } from 'react';
import styles from '../css/MainPage.module.css';
import { Helmet } from 'react-helmet';
import {
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
  TableHead,
  TableSortLabel,
  TableCell,
  TableRow,
  Table,
  TableBody,
  Select,
  MenuItem,
} from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';
import Axios from '../utils/API';
import SearchBar from '../components/SearchBar';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import Pagination from '@material-ui/lab/Pagination';
import Highlighter from 'react-highlight-words';

type header =
  | 'nickname'
  | 'breed'
  | 'gender'
  | 'organization'
  | 'postcode'
  | 'status';
const headers: Array<header> = [
  'nickname',
  'breed',
  'gender',
  'organization',
  'postcode',
  'status',
];

const Adoption = () => {
  const [loading, setLoading] = useState(false);
  const [dog, setDog] = useState(true);
  const [adoptionList, setAdoptionList] = useState<any>([]);
  const [pagination, setPagination] = useState({ page: 0, count: 0 });
  const [page, setPage] = useState(1);
  const [sort, setSort] = useState('');
  const [searchTerm, setSearchTerm] = useState('');

  const search = useLocation().search;

  useEffect(() => {
    setPage(1);
  }, [searchTerm, dog]);

  useEffect(() => {
    setLoading(true);
    if (search) {
      Axios.get('individual_pets/' + search.substr(1))
        .then((res) => {
          setAdoptionList(res.data.individual_pets_of_breed);
          setLoading(false);
        })
        .catch((err) => {
          // didn't find any pets
          setAdoptionList([]);
          setLoading(false);
        });
    } else if (searchTerm) {
      Axios.get('/individual_pets/search', {
        params: { search_for: searchTerm, page: page },
      }).then((res) => {
        setAdoptionList(res.data.search_results);
        setPagination({
          page: res.data.meta_data.page,
          count: res.data.meta_data.total_pages,
        });
        setLoading(false);
      });
    } else {
      Axios.get(dog ? '/individual_dogs' : '/individual_cats', {
        params: { page: page, sort_by: sort ? sort : null },
      }).then((res) => {
        setAdoptionList(
          dog ? res.data.individual_dogs : res.data.individual_cats
        );
        setPagination({
          page: res.data.meta_data.page,
          count: res.data.meta_data.total_pages,
        });
        setLoading(false);
      });
    }
  }, [page, dog, sort, search, searchTerm]);

  return (
    <main>
      <Helmet>
        <title>Adoption</title>
      </Helmet>
      <div className={styles.body}>
        <Loading show={loading} showSpinner={true} color="var(--text-darker)" />
        <div className={styles.top}>
          <h1 className={styles.title}>Pet Adoption List</h1>
          <span className={styles.subtitle}>Find a pet to adopt!</span>
          {!search && <SearchBar setParentSearchTerm={setSearchTerm} />}
          <br />
          <br />
          {search && (
            <Link to="/adoption">
              <Button variant="contained">show full list</Button>
            </Link>
          )}
          {!(search || searchTerm) && (
            <Select
              value={dog ? 'dog' : 'cat'}
              onChange={(event) => {
                event.target.value === 'dog' ? setDog(true) : setDog(false);
              }}
              variant="filled"
            >
              <MenuItem value={'dog'}>Dog</MenuItem>
              <MenuItem value={'cat'}>Cat</MenuItem>
            </Select>
          )}
        </div>
        <Table>
          <TableHead>
            {headers.map((name) => (
              <TableCell key={name}>
                <TableSortLabel
                  active={sort === name}
                  direction="asc"
                  onClick={() => setSort(name)}
                >
                  {name}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableHead>
          <TableBody>
            {adoptionList.map((pet: Pet, index: number) => (
              <TableRow key={index}>
                {headers.map((name) => {
                  return name === 'breed' ? (
                    <TableCell key={name}>
                      <Link
                        style={{ color: 'var(--text-lighter)' }}
                        to={{
                          pathname: '/' + pet.species.toLowerCase() + 's',
                          search: pet[name],
                        }}
                      >
                        <Highlighter
                          searchWords={searchTerm.split(' ')}
                          textToHighlight={pet[name]}
                        />
                      </Link>
                    </TableCell>
                  ) : (
                    <TableCell key={name}>
                      <Highlighter
                        searchWords={searchTerm.split(' ')}
                        textToHighlight={pet[name]}
                      />
                    </TableCell>
                  );
                })}
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <br />
        {adoptionList.length === 0 && <p>The search has returned no result</p>}
        {!(search || searchTerm) && <p>(Click on the table header to sort)</p>}
        {!search && (
          <Pagination
            color="primary"
            page={page}
            count={pagination.count}
            onChange={(event, value) => setPage(value)}
            boundaryCount={2}
            style={{ marginTop: '2em' }}
          />
        )}
      </div>
    </main>
  );
};

interface Pet {
  breed: string;
  gender: string;
  mixed: string;
  nickname: string;
  organization: string;
  postcode: string;
  species: string;
  status: string;
}

export default Adoption;

// import React from 'react';
// import styles from '../css/MainPage.module.css';
// import { Link } from 'react-router-dom';
// import card_img from '../css/Adoption.module.css';
// import {
//   Card,
//   CardMedia,
//   CardContent,
//   CardActionArea,
// } from '@material-ui/core';
// import Pup from '../assets/pup.jpg';
// import Charlie from '../assets/charlie_brown.jpg';
// import Lacy from '../assets/lacy.jpg';

// const adoptPets = ['New Pup', 'Charlie Brown', 'Lacy'];

// const Adoption = () => {
//   return (
//     <div className={styles.body}>
//       <div className="container-fluid">
//         <div className={styles.top}>
//           <h1 className={styles.title}>Pet Adoption List</h1>
//         </div>
//         <div className="row ml-2">
//           <div id="food-card" className="col-sm-9">
//             <div className="row p-2 m-1">
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={Pup}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">Name: New Pup</h3>
//                   <h6 className="card-text">
//                     Breed: Yellow Lab
//                     <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Gender: Female <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Org: ARF of Mercer <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Zip Code: 08638 <br />
//                   </h6>
//                   {/* <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div> */}
//                 </div>
//               </div>
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={Charlie}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">Name: Charlie Brown</h3>
//                   <h6 className="card-text">
//                     Breed: Pitbull/Terrier
//                     <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Gender: Male <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Org: Apollo Support <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Zip Code: 76247 <br />
//                   </h6>
//                   {/* <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div> */}
//                 </div>
//               </div>
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={Lacy}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">Name: Lacy</h3>
//                   <h6 className="card-text">
//                     Breed Siamese <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Gender: Female <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Org: OKC Animal Welfare Division <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Zip Code: 73129
//                     <br />
//                   </h6>
//                   {/* <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div> */}
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Adoption;
