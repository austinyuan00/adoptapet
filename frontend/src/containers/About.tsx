import React, { useState } from 'react';
import styles from '../css/MainPage.module.css';
import { Link } from 'react-router-dom';
import Austin from '../assets/austin_pic.jpg';
import David from '../assets/david.jpg';
import Yoon from '../assets/jungwoongyoon.jpeg';
import Sidharth from '../assets/sidharth.jpg';
import Steve from '../assets/steve_han.jpg';
import {
  Card,
  CardMedia,
  CardContent,
  CardActionArea,
  Button,
} from '@material-ui/core';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import Axios from '../utils/API';
import { Helmet } from 'react-helmet';

const About = () => {
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState('');
  const runTests = () => {
    setLoading(true);
    Axios.get('about/run_test').then((res) => {
      setResult(res.data.result);
      setLoading(false);
      alert('Successfully ran tests! The results are displayed on the page.');
    });
  };
  return (
    <div className={styles.body}>
      <Loading show={loading} showSpinner={true} color="var(--text-darker)" />
      <Helmet>
        <title>About Us</title>
      </Helmet>
      <div className="container-fluid">
        <div className={styles.top}>
          <h1 className={styles.title}>About us</h1>
          <Button
            style={{ marginTop: '1em' }}
            variant="contained"
            onClick={runTests}
          >
            Run Tests
          </Button>
          <p>{result}</p>
        </div>
      </div>
      <style
        dangerouslySetInnerHTML={{
          __html:
            '\n    table, th, td {\n      border: 1px solid black;\n      border-collapse: collapse;\n    }\n    th, td {\n        padding: 5px;\n    }\n    table#t01 {\n      width: 100%;\n      background-color: #f1f1c1;\n    }\n    table#t01 tr:nth-child(even) {\n      background-color: #eee;\n    }\n    table#t01 tr:nth-child(odd) {\n      background-color: #fff;\n    }\n    ',
        }}
      />
      <table id="t01">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Commits pushed</th>
            <th>Issues Closed</th>
            <th>Unittest created</th>
            <th>Total Commits</th>
            <th>Total Issues Closed</th>
            <th>Total Unittest</th>
          </tr>
          <tr>
            <td>Austin Yuan</td>
            <td>15</td>
            <td>8</td>
            <td>6</td>
            <td>31</td>
            <td>34</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Sidharth Eric</td>
            <td>19</td>
            <td>0</td>
            <td>6</td>
            <td>20</td>
            <td>18</td>
            <td>6</td>
          </tr>
          <tr>
            <td>David Yoon</td>
            <td>9</td>
            <td>2</td>
            <td>6</td>
            <td>72</td>
            <td>15</td>
            <td>6</td>
          </tr>
          <tr>
            <td>Steve Han</td>
            <td>39</td>
            <td>13</td>
            <td>6</td>
            <td>74</td>
            <td>21</td>
            <td>6</td>
          </tr>
          <tr>
            <td>David Lambert</td>
            <td>12</td>
            <td>2</td>
            <td>6</td>
            <td>41</td>
            <td>13</td>
            <td>6</td>
          </tr>
        </tbody>
      </table>
      <br />
      <br />
      <br />
      <br />

      <div className="card-deck">
        <div className="card mb-3 bg-light shadow">
          <div className="card-body mt-4">
            <img
              className="card-img-top img-fluid"
              src={Austin}
              alt="profile"
              style={{
                width: '150px',
                height: '150px',
                objectPosition: 'center',
                objectFit: 'cover',
              }}
            />
            <h2 className="card-title">Austin Yuan</h2>
            <p className="card-text">
              Backend
              <br />
              Junior CS undergrad, I like to play badminton
            </p>
          </div>
        </div>
        <div className="card mb-3 bg-light shadow">
          <div className="card-body mt-4">
            <img
              className="card-img-top img-fluid"
              src={David}
              alt="profile"
              style={{
                width: '150px',
                height: '150px',
                objectPosition: 'center',
                objectFit: 'cover',
              }}
            />
            <h2 className="card-title">David Lambert</h2>
            <p className="card-text">
              Frontend
              <br />
              Senior CS Major, I try to take my dog with me everywhere I can.
            </p>
          </div>
        </div>
        <div className="card mb-3 bg-light shadow">
          <div className="card-body mt-4">
            <img
              className="card-img-top img-fluid"
              src={Yoon}
              alt="profile"
              style={{
                width: '150px',
                height: '150px',
                objectPosition: 'center',
                objectFit: 'cover',
              }}
            />
            <h2 className="card-title">David Yoon</h2>
            <p className="card-text">
              Backend
              <br />
              Senior CS Major, I am looking forward to owning a pet.
            </p>
          </div>
        </div>
        <div className="card mb-3 bg-light shadow">
          <div className="card-body mt-4">
            <img
              className="card-img-top img-fluid"
              src={Steve}
              alt="profile"
              style={{
                width: '150px',
                height: '150px',
                objectPosition: 'center',
                objectFit: 'cover',
              }}
            />
            <h2 className="card-title">Steve Han</h2>
            <p className="card-text">
              Frontend
              <br />
              Just finished my 1st year in CS, planning to add music as my
              second major.
            </p>
          </div>
        </div>
        <div className="card mb-3 bg-light shadow">
          <div className="card-body mt-4">
            <img
              className="card-img-top img-fluid"
              src={Sidharth}
              alt="profile"
              style={{
                width: '150px',
                height: '150px',
                objectPosition: 'center',
                objectFit: 'cover',
              }}
            />
            <h2 className="card-title">Sidharth Eric</h2>
            <p className="card-text">
              Backend
              <br />
              Senior CS Major, trying to become a math major too
            </p>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-2"></div>
            <div className="col-lg-2"></div>
          </div>
          <div className="row">
            <div className="col-lg-2"></div>
            <div className="col-lg-8 text-center">
              <br />
              <br />
              <h2>Resources</h2>
            </div>
            <div className="col-lg-2"></div>
            <div className="container">
              <div className="col-lg text-left">
                <div className="col-lg text-center">Namecheap</div>
                <div className="col-lg text-center">AWS</div>
                <div className="col-lg text-center">Flask</div>
                <div className="col-lg text-center">Bootstrap</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="col-lg text-center">
          <a
            href="https://gitlab.com/SteveHan-233/adoptapet"
            className="display-8"
          >
            {' '}
            Gitlab Repo
          </a>
          <br />
          <a
            href="https://gitlab.com/SteveHan-233/adoptapet/-/issues"
            className="display-8"
          >
            Gitlab Issue Tracker
          </a>
          <br />
          <a
            href="https://gitlab.com/SteveHan-233/adoptapet/-/wikis/Technical-Reporth"
            className="display-8"
          >
            Gitlab Wiki
          </a>
          <br />
          <a
            href="https://documenter.getpostman.com/view/11815863/T1DiH1MW?version=latest"
            className="display-8 pull-right"
          >
            {' '}
            Postman{' '}
          </a>
          <br />
          <a
            href="https://speakerdeck.com/seric2020/group-9-presentation"
            className="display-8 pull-right"
          >
            {' '}
            Speaker Deck Presentation{' '}
          </a>
        </div>
        <div className="col-lg text-center">
          <br />
          <h2>Data</h2>
        </div>
        <div className="col-lg text-center">
          <h6>
            {' '}
            We programmatically made get requests to the apis and stored them
            into a json file, which we then used to populate our database.
          </h6>
        </div>
        <div className="container">
          <div className="column">
            <div className="col-lg text-center">
              <a href="https://docs.thecatapi.com/api-reference/breeds/breeds-list#responses">
                Cat API{' '}
              </a>
            </div>
            <div className="col-lg text-center">
              <a href="https://docs.thedogapi.com/api-reference/breeds/breeds-list#responsess">
                Dog API{' '}
              </a>
            </div>
            <div className="col-lg text-center">
              <a href="https://www.petfinder.com/developers/v2/docs/">
                Adoption API{' '}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
