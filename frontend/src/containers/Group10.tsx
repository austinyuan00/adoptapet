import React, { useState, useEffect } from 'react';
import styles from '../css/MainPage.module.css';
import Loading from 'react-loading-bar';
import axios from 'axios';
import PlayersHistogram from '../components/PlayersHistogram';
import { Helmet } from 'react-helmet';

const Group10 = () => {
  const [players, setPlayers] = useState<Player[]>([]);
  // const [teams, setTeams] = useState([]);
  const [games, setGames] = useState<Game[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    Promise.all([
      axios.get('https://hoopscoop.me/api/all/players'),
      // axios.get('https://hoopscoop.me/api/all/teams'),
      axios.get('https://hoopscoop.me/api/all/games'),
    ]).then(([playerRes, gameRes]) => {
      setPlayers(playerRes.data);
      // setTeams(teamRes.data);
      setGames(gameRes.data);
      setLoading(false);
    });
  }, []);

  return (
    <main>
      <Helmet>
        <title>Group 10</title>
      </Helmet>
      <div className={styles.body}>
        <Loading show={loading} showSpinner={true} color="var(--text-darker)" />
        <div className={styles.top}>
          <h1 className={styles.title}>Group 10 API</h1>
          <span className={styles.subtitle}>
            Visualizations we created using the HoopScoop API
          </span>
        </div>
        {players.length > 0 && (
          <>
            <PlayersHistogram
              data={players
                .filter((player) => player.height_inches)
                .map((player) => player.height_inches)}
              title="NBA players' height distribution"
              x="inches"
              y="players"
            />
            <p style={{ textAlign: 'center' }}>
              A histogram showing the amount of players whose heights in inches
              are within a certain range
            </p>
          </>
        )}
        {players.length > 0 && (
          <>
            <PlayersHistogram
              data={players
                .filter((player) => player.weight)
                .map((player) => player.weight)}
              title="NBA players' weight distribution"
              x="lb"
              y="players"
            />
            <p style={{ textAlign: 'center' }}>
              A histogram showing the amount of players whose weights in lb are
              within a certain range
            </p>
          </>
        )}
        {players.length > 0 && (
          <>
            <PlayersHistogram
              data={players
                .filter((player) => player.gamesplayed)
                .map((player) => player.gamesplayed)}
              title="NBA players' number of games played distribution"
              x="games"
              y="players"
            />
            <p style={{ textAlign: 'center' }}>
              A histogram showing the amount of players plotted against the
              number of games they have played
            </p>
          </>
        )}
        {games.length > 0 && (
          <>
            <PlayersHistogram
              data={games.map((game) =>
                Math.abs(game.awayTeamScore - game.homeTeamScore)
              )}
              title="Score difference in games distribution"
              x="games"
              y="score difference"
            />
            <p style={{ textAlign: 'center' }}>
              A histogram showing the distribution of the score differences in
              games
            </p>
          </>
        )}
      </div>
    </main>
  );
};

export interface Player {
  id: number;
  name: string;
  position: string;
  height: string;
  height_inches: number;
  weight: number;
  gamesplayed: number;
  pointspg: number;
  reboundspg: number;
  assistspg: number;
  stealspg: number;
  blockspg: number;
  teamId: number;
}

export interface Game {
  id: number;
  date: string;
  homeTeamId: number;
  homeTeamScore: number;
  awayTeamId: number;
  awayTeamScore: number;
}

export interface Team {
  id: number;
  name: string;
  abbr: string;
  city: string;
  conference: string;
  division: string;
  playerIds: number[];
}

export default Group10;
