import React, { useState, useEffect, useRef } from 'react';
import styles from '../css/MainPage.module.css';
import {
  Card,
  CardMedia,
  CardContent,
  CardActionArea,
  CardActions,
  Button,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from '@material-ui/core';
import { Link, useLocation } from 'react-router-dom';
import Axios from '../utils/API';
import SearchBar from '../components/SearchBar';
import Loading from 'react-loading-bar';
import 'react-loading-bar/dist/index.css';
import Pagination from '@material-ui/lab/Pagination';
import Highlighter from 'react-highlight-words';
import { Helmet } from 'react-helmet';

const Cats = () => {
  const [loading, setLoading] = useState(false);
  const [catBreeds, setCatBreeds] = useState<any>([]);
  const [pagination, setPagination] = useState({ page: 0, count: 0 });
  const [page, setPage] = useState(1);
  const [sort, setSort] = useState('name');
  const [searchTerm, setSearchTerm] = useState('');

  const location = useLocation();

  useEffect(() => {
    setLoading(true);

    if (searchTerm) {
      Axios.get('/cat_breeds/search', {
        params: { search_for: searchTerm },
      }).then((res) => {
        setCatBreeds([...res.data.and_results, ...res.data.or_results]);
        setLoading(false);
      });
    } else {
      Axios.get(
        location.search
          ? '/cat_breeds/' + location.search.substr(1)
          : '/cat_breeds',
        {
          params: {
            page: location.search ? null : page,
            sort_by: location.search ? null : sort || null,
          },
        }
      ).then((res) => {
        setCatBreeds(res.data.cat_breeds);
        !location.search &&
          setPagination({
            page: res.data.meta_data.page,
            count: res.data.meta_data.total_pages,
          });
        setLoading(false);
      });
    }
  }, [page, location.search, sort, searchTerm]);

  return (
    <main>
      <Helmet>
        <title>Cat Breeds</title>
      </Helmet>
      <div className={styles.body}>
        <Loading show={loading} showSpinner={true} color="var(--text-darker)" />
        <div className={styles.top}>
          <h1 className={styles.title}>Cat Breeds</h1>
          <span className={styles.subtitle}>
            Which breed of cats would you like to adopt?
          </span>
          {!location.search && (
            <SearchBar setParentSearchTerm={setSearchTerm} />
          )}
          <br />
          <br />
          {/* <FormControl variant="filled">
            <InputLabel id="sort">Sort by</InputLabel> */}
          {/* </FormControl> */}
          {location.search && (
            <Link to="/cats">
              <Button variant="contained">show full list</Button>
            </Link>
          )}
          {!(location.search || searchTerm) && (
            <>
              <p style={{ marginBottom: '5px' }}>Sort by: </p>
              <Select
                labelId="sort"
                value={sort}
                onChange={(event) => setSort(event.target.value + '')}
                variant="filled"
                style={{ marginBottom: '2em' }}
              >
                <MenuItem value="">None</MenuItem>
                <MenuItem value={'name'}>Name</MenuItem>
                <MenuItem value={'temperament'}>Temperament</MenuItem>
                <MenuItem value={'life_span'}>Life span</MenuItem>
                <MenuItem value={'weight_imperial'}>Weight (imperial)</MenuItem>
                <MenuItem value={'weight_metric'}>Weight (metric)</MenuItem>
              </Select>
              <Pagination
                color="primary"
                page={page}
                count={pagination.count}
                onChange={(event, value) => setPage(value)}
                boundaryCount={2}
              />
            </>
          )}
        </div>
        <div className={styles.cards_container}>
          {catBreeds.map((cat: Cat, id: number) => (
            <Card key={id}>
              <CardMedia image={cat.image_url} />
              <CardContent>
                <span className={styles.card_title}>
                  <Highlighter
                    searchWords={searchTerm.split(' ')}
                    textToHighlight={cat.name}
                  />
                </span>
                <div className={styles.card_description}>
                  <div className={styles.row}>
                    <h3>Temperament: </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={cat.temperament}
                    />{' '}
                  </div>
                  <div className={styles.row}>
                    <h3>Life span: </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={cat.life_span}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Weight (imperial): </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={cat.weight_imperial}
                    />
                  </div>
                  <div className={styles.row}>
                    <h3>Weight (metric): </h3>
                    <Highlighter
                      searchWords={searchTerm.split(' ')}
                      textToHighlight={cat.weight_metric}
                    />
                  </div>
                  <div className={styles.row}>
                    <a
                      href={cat.wikipedia_url}
                      style={{ margin: 0, textDecoration: 'underline' }}
                    >
                      <h3>Wikipedia Link</h3>
                    </a>
                  </div>
                </div>
              </CardContent>
              <CardActions
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  marginBottom: '2em',
                }}
              >
                <Link
                  to={{ pathname: '/adoption', search: cat.name }}
                  style={{
                    display: 'block',
                  }}
                >
                  <Button variant="contained">Find one to adopt</Button>
                </Link>
              </CardActions>
            </Card>
          ))}
        </div>
        {!(location.search || searchTerm) && (
          <Pagination
            color="primary"
            page={page}
            count={pagination.count}
            onChange={(event, value) => setPage(value)}
            boundaryCount={2}
          />
        )}
      </div>
    </main>
  );
};

interface Cat {
  wikipedia_url: string;
  image_url: string;
  life_span: string;
  name: string;
  temperament: string;
  weight_imperial: string;
  weight_metric: string;
}

export default Cats;

// import React from 'react';
// import styles from '../css/MainPage.module.css';
// import { Link } from 'react-router-dom';
// import {
//   Card,
//   CardMedia,
//   CardContent,
//   CardActionArea,
// } from '@material-ui/core';
// import American from '../assets/american_wirehair.jpeg';
// import Abyssinian from '../assets/abyssinian.png';
// import Siamese from '../assets/Siamese.jpg';

// const catBreeds = ['Persian Cat', 'Maine Coon', 'Ragdoll'];

// const Cats = () => {
//   return (
//     <div className={styles.body}>
//       <div className="container-fluid">
//         <div className={styles.top}>
//           <h1 className={styles.title}>Cat Breeds</h1>
//         </div>
//         <div className="row ml-2">
//           <div id="food-card" className="col-sm-9">
//             <div className="row p-2 m-1">
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={Siamese}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">Siamese Cat</h3>
//                   <h6 className="card-text">
//                     Temperment: Active
//                     <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Life Span: 12 years <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Metric Weight: 4lbs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Imperial Weight: 8kgs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Wiki:{' '}
//                     <a href="https://en.wikipedia.org/wiki/Siamese_(cat)">
//                       Siamese Cat
//                     </a>{' '}
//                     <br />
//                   </h6>
//                   <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={American}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">American Wirehair</h3>
//                   <h6 className="card-text">
//                     Temperment: Clever
//                     <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Life Span: 12 years <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Metric Weight: 5lbs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Imperial Weight: 9kgs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Wiki:{' '}
//                     <a href="https://en.wikipedia.org/wiki/American_Wirehair">
//                       American Wirehair
//                     </a>{' '}
//                     <br />
//                   </h6>
//                   <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//               <div className="col-lg-3 card shadow-sm bg-light text-center p-2 m-2">
//                 <img
//                   className="card-img-top"
//                   style={{
//                     height: '200px',
//                     objectFit: 'cover',
//                     objectPosition: 'center',
//                   }}
//                   src={Abyssinian}
//                   alt="Card image cap"
//                 />
//                 <div className="card-body">
//                   <h3 className="card-title">Abyssinian</h3>
//                   <h6 className="card-text">
//                     Temperment: Independent
//                     <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Life Span: 14 years <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Metric Weight: 6lbs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Imperial Weight: 10kgs <br />
//                   </h6>
//                   <h6 className="card-text">
//                     Wiki:{' '}
//                     <a href="https://en.wikipedia.org/wiki/Abyssinian_(cat)">
//                       Abyssinian
//                     </a>{' '}
//                     <br />
//                   </h6>
//                   <div className="row p-2 m-1 shadow-sm bg-light">
//                     <div className="col-12 text-center">
//                       <Link
//                         to="/adoption"
//                         className="btn btn-primary mt-5 mb-5"
//                         role="button"
//                       >
//                         Find in shelter
//                       </Link>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
//   // return (
//   //   <main>
//   //     <div className={styles.top}>
//   //       <h1 className={styles.title}>Cat Breeds</h1>
//   //       <span className={styles.subtitle}>
//   //         Which breed of cats would you like to adopt?
//   //       </span>
//   //     </div>
//   //     <div className={styles.cards_container}>
//   //       {catBreeds.map((breed) => (
//   //         <Card>
//   //           <Link to="/shelters">
//   //             <CardActionArea>
//   //               <CardMedia image="https://upload.wikimedia.org/wikipedia/commons/6/64/Ragdoll_from_Gatil_Ragbelas.jpg" />
//   //               <CardContent>
//   //                 <span className={styles.card_title}>{breed}</span>
//   //               </CardContent>
//   //             </CardActionArea>
//   //           </Link>
//   //         </Card>
//   //       ))}
//   //     </div>
//   //   </main>
//   // );
// };

// export default Cats;
