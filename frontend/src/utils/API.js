import axios from 'axios';

export default axios.create({
  baseURL: 'http://adoptapet.me:80',
  // baseURL: 'http://localhost',
  responseType: 'json',
});
