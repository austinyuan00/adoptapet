#!/usr/bin/env python3

from models import app, db, DogBreed, CatBreed, IndividualDog, IndividualCat
from flask_sqlalchemy import SQLAlchemy
# from sqlalchemy_utils import dependent_objects
import json


def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


def create_breeds():
    dog_breeds = load_json('dog-breeds.json')
    cat_breeds = load_json('cat-breeds.json')
    dog_urls = load_json('dogBreedImageURLs.json')
    cat_urls = load_json('catBreedImageURLs.json')
    # Store dog breed data
    for breed in dog_breeds:
        name = breed['name']

        # Some dog breeds do not have temperament attributes
        try:
            temperament = breed['temperament']
        except KeyError:
            temperament = ""

        life_span = breed['life_span']
        weight_metric = breed['weight']['metric']
        weight_imperial = breed['weight']['imperial']
        height_metric = breed['height']['metric']
        height_imperial = breed['height']['imperial']
        image_url = dog_urls[name]

        new_dog_breed = DogBreed(
            name = name,
            temperament = temperament,
            life_span = life_span,
            weight_metric = weight_metric,
            weight_imperial = weight_imperial,
            height_metric = height_metric,
            height_imperial = height_imperial,
            image_url = image_url
        )

        db.session.add(new_dog_breed)
        db.session.commit()

    # Store cat breed data
    for breed in cat_breeds:
        name = breed['name']
        temperament = breed['temperament']
        life_span = breed['life_span']
        weight_metric = breed['weight']['metric']
        weight_imperial = breed['weight']['imperial']
        image_url = cat_urls[name]

        # Some cat breeds do not have wikipedia_url attributes
        try:
            wikipedia_url = breed['wikipedia_url']
        except KeyError:
            wikipedia_url = ""

        new_cat_breed = CatBreed(
            name = name,
            temperament = temperament,
            life_span = life_span,
            weight_metric = weight_metric,
            weight_imperial = weight_imperial,
            wikipedia_url = wikipedia_url,
            image_url = image_url
        )

        db.session.add(new_cat_breed)
        db.session.commit()


def create_individuals():
    individual_dogs = load_json('dogs.json')
    individual_cats = load_json('cats.json')
    organizations = load_json('orgsById.json')
    no_orgs_animals = load_json('animalsWithMissingOrg.json')

    # Store individual dog data
    for dog in individual_dogs:
        if dog['id'] not in no_orgs_animals:
            nickname = dog['name']

            # linking to dog breed
            breed = dog['breeds']['primary']
            breed_match = db.session.query(DogBreed).filter_by(name=breed).first()

            mixed = str(dog['breeds']['mixed'])
            species = dog['species']
            status = dog['status']
            gender = dog['gender']

            # Get an organization id, so that we can get the name and postcode with this
            org_id = dog['organization_id']

            try:
                organization = organizations[org_id][0]
            except:
                organization = ""

            try:
                postcode = organizations[org_id][1]
            except:
                postcode = ""
            
            # Add to the database only if it matches with the breed data
            if breed_match != None:
                new_dog = IndividualDog(
                    nickname = nickname, 
                    #breed = breed,
                    dog_breed = breed_match,
                    mixed = mixed,
                    species = species,
                    status = status,
                    gender = gender,
                    organization = organization,
                    postcode = postcode
                )

                db.session.add(new_dog)
                db.session.commit()


    # Store individual cat data
    for cat in individual_cats:
        if cat['id'] not in no_orgs_animals:
            nickname = cat['name']

            # linking to the cat breed
            breed = cat['breeds']['primary']
            breed_match = db.session.query(CatBreed).filter_by(name=breed).first()

            mixed = str(cat['breeds']['mixed'])
            species = cat['species']
            status = cat['status']
            gender = cat['gender']

            # Get an organization id, so that we can get the name and postcode with this
            org_id = cat['organization_id']

            try:
                organization = organizations[org_id][0]
            except:
                organization = ""

            try:
                postcode = organizations[org_id][1]
            except:
                postcode = ""
            
            # Add to the database only if it matches with the breed data
            if breed_match != None:
                new_cat = IndividualCat(
                    nickname = nickname, 
                    # breed = breed,
                    cat_breed = breed_match,
                    mixed = mixed,
                    species = species,
                    status = status,
                    gender = gender,
                    organization = organization,
                    postcode = postcode
                )

                db.session.add(new_cat)
                db.session.commit()


# if __name__ == "__main__":
#     create_breeds()
#     create_individuals()
