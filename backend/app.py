from flask import Flask, render_template, jsonify, Response, send_from_directory, request
from flask_sqlalchemy import SQLAlchemy
import requests
import json
import os
import sqlalchemy_utils
from sqlalchemy_utils import dependent_objects
from models import app, db, DogBreed, CatBreed, IndividualDog, IndividualCat
import tests
import subprocess

# List of dog breeds


@app.route('/dog_breeds', methods=['GET'])
def dog_breeds():
    sort_by = request.args.get('sort_by')
    page = request.args.get('page', 1, type=int)

    try:
        result = db.session.query(DogBreed).order_by(
            sort_by).paginate(page, 20, False)
    except:
        result = []

    return jsonify(dog_breeds=[r.serialize for r in result.items], meta_data={"page": result.page, "total_pages": result.pages, "total_items": result.total})


# List of cat breeds
@app.route('/cat_breeds', methods=['GET'])
def cat_breeds():
    sort_by = request.args.get('sort_by')
    page = request.args.get('page', 1, type=int)

    try:
        result = db.session.query(CatBreed).order_by(
            sort_by).paginate(page, 20, False)
    except:
        result = []

    return jsonify(cat_breeds=[r.serialize for r in result.items], meta_data={"page": result.page, "total_pages": result.pages, "total_items": result.total})


# search dog breeds
@app.route('/dog_breeds/<keyword>', methods=['GET'])
def dog_breeds_search(keyword):
    result = db.session.query(DogBreed).filter_by(name=keyword)
    return jsonify(dog_breeds=[r.serialize for r in result])


# search cat breeds
@app.route('/cat_breeds/<keyword>', methods=['GET'])
def cat_breeds_search(keyword):
    result = db.session.query(CatBreed).filter_by(name=keyword)
    return jsonify(cat_breeds=[r.serialize for r in result])


# List of individual pets
@app.route('/individual_pets', methods=['GET'])
def individual_pets():
    page = request.args.get('page', 1, type=int)

    dog_result = db.session.query(
        IndividualDog).paginate(page, 20, False).items
    cat_result = db.session.query(
        IndividualCat).paginate(page, 20, False).items

    return jsonify(individual_dogs=[r.serialize for r in dog_result],
                   individual_cats=[r.serialize for r in cat_result])


# List of all individual dogs
@app.route('/individual_dogs', methods=['GET'])
def individual_dogs():
    sort_by = request.args.get('sort_by')
    page = request.args.get('page', 1, type=int)
    result = db.session.query(
        IndividualDog).order_by(sort_by).paginate(page, 20, False)
    return jsonify(individual_dogs=[r.serialize for r in result.items], meta_data={"page": result.page, "total_pages": result.pages, "total_items": result.total})


# List of all individual cats
@app.route('/individual_cats', methods=['GET'])
def individual_cats():
    sort_by = request.args.get('sort_by')
    page = request.args.get('page', 1, type=int)
    result = db.session.query(
        IndividualCat).order_by(sort_by).paginate(page, 20, False)
    return jsonify(individual_cats=[r.serialize for r in result.items], meta_data={"page": result.page, "total_pages": result.pages, "total_items": result.total})


# list individual pets by breed
@app.route('/individual_pets/<breed>', methods=['GET'])
def filter_individual_pets(breed):
    page = request.args.get('page', 1, type=int)
    sort_by = request.args.get('sort_by')
    dog_result = db.session.query(IndividualDog).filter_by(
        breed=breed).order_by(sort_by).paginate(page, 20, False)
    cat_result = db.session.query(IndividualCat).filter_by(
        breed=breed).order_by(sort_by).paginate(page, 20, False)
    if len(dog_result.items) != 0:
        return jsonify(individual_pets_of_breed=[r.serialize for r in dog_result.items], metadata={"page": dog_result.page, "total_pages": dog_result.pages, "total_items": dog_result.total})
    if len(cat_result.items) != 0:
        return jsonify(individual_pets_of_breed=[r.serialize for r in cat_result.items], metadata={"page": cat_result.page, "total_pages": cat_result.pages, "total_items": cat_result.total})


# Run test on the about page
@app.route('/about/run_test', methods=['GET'])
def run_test():
    result = subprocess.check_output(
        ['python3', 'tests.py'], stderr=subprocess.STDOUT, universal_newlines=True)

    return jsonify(result=str(result))


# Search functionality for dog breeds
@app.route('/dog_breeds/search', methods=['GET'])
def search_for_dog_breeds():
    search_for = request.args.get('search_for')
    word_list = search_for.split()

    # For the test purpose
    # word_list = ["alert", "courageous", "Loving"]

    queries = []

    for word in word_list:
        for col in DogBreed.__table__.columns:
            queries.append(db.session.query(DogBreed).filter(
                col.ilike("%" + word + "%")))

    temp_and_results = []
    or_results = []
    or_results_set = []

    for query in queries:
        for r in query:
            breed = r.serialize
            if breed not in or_results_set:
                or_results_set.append(breed)
            if breed in or_results and breed not in temp_and_results:
                temp_and_results.append(breed)
            # elif breed not in and_results:
            or_results.append(breed)

    and_results = []

    for and_result in temp_and_results:
        count = or_results.count(and_result)

        if count == len(word_list):
            and_results.append(and_result)

    return jsonify(and_results=and_results, or_results=or_results_set)


# Search functionality for cat breeds
@app.route('/cat_breeds/search', methods=['GET'])
def search_for_cat_breeds():
    search_for = request.args.get('search_for')
    word_list = search_for.split()

    # For the test purpose
    # word_list = ["Abyssinian", "gentle"]

    queries = []

    for word in word_list:
        for col in CatBreed.__table__.columns:
            queries.append(db.session.query(CatBreed).filter(
                col.ilike("%" + word + "%")))

    temp_and_results = []
    or_results = []
    or_results_set = []

    for query in queries:
        for r in query:
            breed = r.serialize
            if breed not in or_results_set:
                or_results_set.append(breed)
            if breed in or_results and breed not in temp_and_results:
                temp_and_results.append(breed)
            # elif breed not in and_results:
            or_results.append(breed)

    and_results = []

    for and_result in temp_and_results:
        count = or_results.count(and_result)

        if count == len(word_list):
            and_results.append(and_result)

    return jsonify(and_results=and_results, or_results=or_results_set)


# search functionality for individual pets
@app.route('/individual_pets/search', methods=['GET'])
def search_for_individual_pets():
    search_for = request.args.get('search_for')
    word_list = search_for.split()
    page = request.args.get('page', 1, type=int)
    assert(page > 0)

    # test case
    # word_list = ["Animal Protective League", "62702"]

    queries = []

    for word in word_list:
        for col in IndividualDog.__table__.columns:
            if col != IndividualDog.id:
                queries.append(db.session.query(IndividualDog).filter(
                    col.ilike("%" + word + "%")))
        for col in IndividualCat.__table__.columns:
            if col != IndividualCat.id:
                queries.append(db.session.query(IndividualCat).filter(
                    col.ilike("%" + word + "%")))

    temp_and_results = []
    or_results = []
    or_results_set = []

    for query in queries:
        for r in query:
            individual = r.serialize
            if individual not in or_results_set:
                or_results_set.append(individual)
            if individual in or_results and individual not in temp_and_results:
                temp_and_results.append(individual)
            or_results.append(individual)

    and_results = []

    for result in temp_and_results:
        if or_results.count(result) == len(word_list):
            and_results.append(result)

    combined_results = and_results + or_results
    total_items = len(combined_results)
    paginated_results = [combined_results[i:i+20]
                         for i in range(0, len(combined_results), 20)]
    total_pages = len(paginated_results)

    return jsonify(search_results=paginated_results[page - 1], meta_data={"page": page, "total_pages": total_pages, "total_items": total_items})


### Group 10's APIs ###

# Get a list of ids for players in the NBA
@app.route('/group_10/ids', methods=['GET'])
def get_player_ids():
    url = "https://hoopscoop.me/api/players"
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get a page of player stats
@app.route('/group_10/players/page/<number>', methods=['GET'])
def player_stats_page(number):
    url = "https://hoopscoop.me/api/players/page/" + str(number)
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get a stats for the player with the given id
@app.route('/group_10/players/<id>', methods=['GET'])
def get_player_stats(id):
    url = "https://hoopscoop.me/api/players/id/" + str(id)
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get stats for all the teams in the NBA
@app.route('/group_10/teams', methods=['GET'])
def get_team_ids():
    url = "https://hoopscoop.me/api/teams"
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get stats for the team with the given id
@app.route('/group_10/teams/<id>', methods=['GET'])
def get_team_stats(id):
    url = "https://hoopscoop.me/api/teams/id/" + str(id)
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get a a list of ids for games in the NBA
@app.route('/group_10/games', methods=['GET'])
def get_game_ids():
    url = "https://hoopscoop.me/api/games"
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get a page of game stats
@app.route('/group_10/games/page/<number>', methods=['GET'])
def game_stats_page(number):
    url = "https://hoopscoop.me/api/games/page/" + str(number)
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


# Get stats for the game with the given id
@app.route('/group_10/games/<id>', methods=['GET'])
def get_game_stats(id):
    url = "https://hoopscoop.me/api/games/id/" + str(id)
    response = requests.get(url)
    data = response.json()

    return jsonify(data)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    """
    Serve pages to the frontend.
    Path specifices which page is being requested.
    return correct page
    """
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int("80"), debug=True)
