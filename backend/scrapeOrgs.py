import json
import requests

'''
Scrapes every organization in Petfinder API and saves it to orgs.json
'''
def scrape(auth):
    headers = {
        'Authorization': auth,
    }

    params = (
        ('page', '1'),
        ('limit', '100'),
    )
    orgs = []
    response = requests.get('https://api.petfinder.com/v2/organizations', headers=headers, params=params)
    curDict = response.json()
    for org in curDict["organizations"]:
        orgs.append(org)
    numPages = curDict['pagination']['total_pages']
    curPage = 2
    while curPage <= numPages:
        params = (
            ('page', str(curPage)),
            ('limit', '100'),
        )
        response = requests.get('https://api.petfinder.com/v2/organizations', headers=headers, params=params)
        curDict = response.json()
        for org in curDict["organizations"]:
            orgs.append(org)
        curPage += 1

    with open('orgs.json', 'w') as json_file:
        json.dump(orgs, json_file, indent = 4)


# Hardcoded auth token:
auth = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJuN2t0am5YNEp1VWdXbnNNVkozV3hic2t5Wjg1dW1CcDg5Z1dReDk4UGYyamZkem1EaiIsImp0aSI6IjcwNWEwM2VmOWJkNDM2YzE0ZGM2NDNiMDhkZDI1NTNjMjNiM2Q2NGRiOTJkNDNlOWZlNTBlOGQ0YTZjYzMyNWRlNmY5MzE5ZTZiYzJlZDJkIiwiaWF0IjoxNTkzODk4NjA2LCJuYmYiOjE1OTM4OTg2MDYsImV4cCI6MTU5MzkwMjIwNiwic3ViIjoiIiwic2NvcGVzIjpbXX0.PQewV1547cvGBRZaLk3WVreGTtDQf-ptYRCXy-FxEdYSoQ25WQ3ajDPm0Pb9uCWhQ4Iavh2ICL6qCTwLS9mEOh8WPq68h9d66eGuy9zo_iONgH4wRBjOhidECAfyqkQTPpfVkeTGN2TOtOzent-RsXGoqe7jPkVxR40cyvijwiTWMYl1iA_rbKTVmiTJVl0rnWHdaq3VUwmRigkPIwZAfvfrnASdbYWssLcw75IrSmnNIEhXN8nLyTxFn7DkpyY0htnzHtoOJLcE9xi_j25jYCT52l5bbozuTJKzfxNOn6eKzZyOIohs868fowKJd04gVufMqg2Um5ozyZwUVteVgg'

scrape(auth)


