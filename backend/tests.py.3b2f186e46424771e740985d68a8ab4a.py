import os
import sys
import unittest
import requests

from models import db, DogBreed, CatBreed, IndividualCat, IndividualDog

class TestModel (unittest.TestCase):
# ----
# DogBreed
# ----

    def test_dog_breeds_1(self):
        new_dog_breed = DogBreed(
            name = "Fake Breed",
            temperament = "Wild",
            life_span = "12 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            height_metric = "53 - 58",
            height_imperial = "21 - 23",
            image_url = "fakeurl.com"
        )

        db.session.add(new_dog_breed)

        r = db.session.query(DogBreed).filter_by(name = "Fake Breed").one()
        self.assertEqual(r.name, "Fake Breed")
        self.assertEqual(r.life_span, "12 years")

        db.session.delete(new_dog_breed)

    def test_dog_breeds_2(self):
        new_dog_breed = DogBreed(
            name = "Breed XYZ",
            temperament = "Unique temperament",
            life_span = "12 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            height_metric = "53 - 58",
            height_imperial = "21 - 23",
            image_url = "fakeurl.com"
        )

        db.session.add(new_dog_breed)

        r = db.session.query(DogBreed).filter_by(temperament = "Unique temperament").one()
        self.assertEqual(r.temperament, "Unique temperament")
        self.assertEqual(r.name, "Breed XYZ")
        serialized = {
            "name": "Breed XYZ",
            "temperament": "Unique temperament",
            "life_span": "12 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "height_metric": "53 - 58",
            "height_imperial": "21 - 23",
            "image_url": "fakeurl.com"
        }
        self.assertEqual(r.serialize, serialized)
        db.session.delete(new_dog_breed)

    def test_dog_breeds_3(self):
        new_dog_breed = DogBreed(
            name = "Breed #3",
            temperament = "Unique temperament",
            life_span = "12 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            height_metric = "53 - 58",
            height_imperial = "21 - 23",
            image_url = "fakeurl.com"
        )

        db.session.add(new_dog_breed)

        r = db.session.query(DogBreed).filter_by(temperament = "Unique temperament", life_span = "12 years", image_url = "fakeurl.com").one()
        self.assertEqual(r.life_span, "12 years")
        self.assertEqual(r.name, "Breed #3")
        serialized = {
            "name": "Breed #3",
            "temperament": "Unique temperament",
            "life_span": "12 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "height_metric": "53 - 58",
            "height_imperial": "21 - 23",
            "image_url": "fakeurl.com"
        }
        self.assertEqual(r.serialize, serialized)
        db.session.delete(new_dog_breed)

    def test_dog_breeds_4(self):
        new_dog_breed = DogBreed(
            name = "Breed #4",
            temperament = "Malding",
            life_span = "1 years",
            weight_metric = "9001 - 290000",
            weight_imperial = "40 - 65",
            height_metric = "53 - 58",
            height_imperial = "21 - 23",
            image_url = "fakeurl.com"
        )

        db.session.add(new_dog_breed)

        r = db.session.query(DogBreed).filter_by(temperament = "Malding", life_span = "1 years", image_url = "fakeurl.com").one()
        self.assertEqual(r.life_span, "1 years")
        self.assertEqual(r.name, "Breed #4")
        serialized = {
            "name": "Breed #4",
            "temperament": "Malding",
            "life_span": "1 years",
            "weight_metric": "9001 - 290000",
            "weight_imperial": "40 - 65",
            "height_metric": "53 - 58",
            "height_imperial": "21 - 23",
            "image_url": "fakeurl.com"
        }
        self.assertEqual(r.serialize, serialized)
        db.session.delete(new_dog_breed)

# -----
# CatBreed
# -----

    def test_cat_breeds_1(self):
        new_cat_breed = CatBreed(
            name = "Test Name Cat",
            temperament = "Wild",
            life_span = "30 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            wikipedia_url = "fakeWikiUrl.com",
            image_url = "fakeImageUrl.com"
        )

        db.session.add(new_cat_breed)

        r = db.session.query(CatBreed).filter_by(name = "Test Name Cat").one()
        self.assertEqual(r.name, "Test Name Cat")
        self.assertEqual(r.life_span, "30 years")
        serialized = {
            "name": "Test Name Cat",
            "temperament": "Wild",
            "life_span": "30 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "wikipedia_url": "fakeWikiUrl.com",
            "image_url": "fakeImageUrl.com"
        }
        self.assertEqual(r.serialize, serialized)

        db.session.delete(new_cat_breed)

    def test_cat_breeds_2(self):
        new_cat_breed = CatBreed(
            name = "Test Name Cat",
            temperament = "Wild",
            life_span = "30 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            wikipedia_url = "fakeWikiUrl.com",
            image_url = "fakeImageUrl.com"
        )

        db.session.add(new_cat_breed)

        r = db.session.query(CatBreed).filter_by(image_url = "fakeImageUrl.com").one()
        self.assertEqual(r.name, "Test Name Cat")
        self.assertEqual(r.life_span, "30 years")
        serialized = {
            "name": "Test Name Cat",
            "temperament": "Wild",
            "life_span": "30 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "wikipedia_url": "fakeWikiUrl.com",
            "image_url": "fakeImageUrl.com"
        }
        self.assertEqual(r.serialize, serialized)

        db.session.delete(new_cat_breed)

    def test_cat_breeds_3(self):
        new_cat_breed = CatBreed(
            name = "cat #3",
            temperament = "Wild",
            life_span = "30 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            wikipedia_url = "fakeWikiUrl.com",
            image_url = "fakeImageUrl.com"
        )

        db.session.add(new_cat_breed)
        r = db.session.query(CatBreed).filter_by(image_url = "fakeImageUrl.com", name = "cat #3", life_span = "30 years").one()
        self.assertEqual(r.name, "cat #3")
        self.assertEqual(r.life_span, "30 years")
        serialized = {
            "name": "cat #3",
            "temperament": "Wild",
            "life_span": "30 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "wikipedia_url": "fakeWikiUrl.com",
            "image_url": "fakeImageUrl.com"
        }
        self.assertEqual(r.serialize, serialized)

        db.session.delete(new_cat_breed)

    def test_cat_breeds_4(self):
        new_cat_breed = CatBreed(
            name = "cat #4",
            temperament = "angrey",
            life_span = "200 years",
            weight_metric = "18 - 29",
            weight_imperial = "40 - 65",
            wikipedia_url = "fakeWikiUrl.com",
            image_url = "fakeImageUrl.com"
        )

        db.session.add(new_cat_breed)
        r = db.session.query(CatBreed).filter_by(image_url = "fakeImageUrl.com", name = "cat #4", life_span = "200 years").one()
        self.assertEqual(r.name, "cat #4")
        self.assertEqual(r.life_span, "200 years")
        serialized = {
            "name": "cat #4",
            "temperament": "angrey",
            "life_span": "200 years",
            "weight_metric": "18 - 29",
            "weight_imperial": "40 - 65",
            "wikipedia_url": "fakeWikiUrl.com",
            "image_url": "fakeImageUrl.com"
        }
        self.assertEqual(r.serialize, serialized)

        db.session.delete(new_cat_breed)


class ModelsTestCases(unittest.TestCase):
    def test_individual_dog_1(self):
        new_dog = IndividualDog(
                    nickname = "Me",
                    dog_breed = db.session.query(DogBreed).filter_by(name="Akita").first(),
                    mixed = "False",
                    species = "Dog",
                    status = "Adoptable",
                    gender = "Male",
                    organization = "An Organization",
                    postcode = "78701"
                )

        db.session.add(new_dog)
        db.session.commit()

        result = db.session.query(IndividualDog).filter_by(nickname = "Me", breed = 'Akita').one()
        self.assertEqual(result.breed, 'Akita')
        self.assertEqual(result.status, 'Adoptable')
        self.assertEqual(result.nickname, 'Me')

        serialized = {
            'nickname' : "Me",
            'breed' : "Akita",
            'mixed' : "False",
            'species' : "Dog",
            'status' : "Adoptable",
            'gender' : "Male",
            'organization' : "An Organization",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualDog).filter_by(nickname = "Me", breed = 'Akita').delete()
        db.session.commit()

    def test_individual_dog_2(self):
        new_dog = IndividualDog(
                    nickname = "You",
                    dog_breed = db.session.query(DogBreed).filter_by(name="Golden Retriever").first(),
                    mixed = "False",
                    species = "Dog",
                    status = "Adoptable",
                    gender = "Male",
                    organization = "The Organization",
                    postcode = "78701"
                )

        db.session.add(new_dog)
        db.session.commit()

        result = db.session.query(IndividualDog).filter_by(nickname = "You", breed = 'Golden Retriever').one()
        self.assertEqual(result.breed, 'Golden Retriever')
        self.assertEqual(result.nickname, 'You')

        serialized = {
            'nickname' : "You",
            'breed' : "Golden Retriever",
            'mixed' : "False",
            'species' : "Dog",
            'status' : "Adoptable",
            'gender' : "Male",
            'organization' : "The Organization",
            "postcode" : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualDog).filter_by(nickname = "You", breed = 'Golden Retriever').delete()
        db.session.commit()

    def test_individual_dog_3(self):
        new_dog = IndividualDog(
                    nickname = "Us",
                    dog_breed = db.session.query(DogBreed).filter_by(name="French Bulldog").first(),
                    mixed = "True",
                    species = "Dog",
                    status = "Adoptable",
                    gender = "Male",
                    organization = "Adopt A Pet",
                    postcode = "78701"
                )

        db.session.add(new_dog)
        db.session.commit()

        result = db.session.query(IndividualDog).filter_by(nickname = "Us", breed = 'French Bulldog').one()
        self.assertEqual(result.breed, 'French Bulldog')
        self.assertEqual(result.nickname, 'Us')
        self.assertEqual(result.mixed, 'True')

        serialized = {
            'nickname' : "Us",
            'breed' : "French Bulldog",
            'mixed' : "True",
            'species' : "Dog",
            'status' : "Adoptable",
            'gender' : "Male",
            'organization' : "Adopt A Pet",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualDog).filter_by(nickname = "Us", breed = 'French Bulldog').delete()
        db.session.commit()

    def test_individual_dog_4(self):
        new_dog = IndividualDog(
                    nickname = "Nina",
                    dog_breed = db.session.query(DogBreed).filter_by(name="Great Pyrenees").first(),
                    mixed = "True",
                    species = "Dog",
                    status = "Adoptable",
                    gender = "Female",
                    organization = "Adopt A Pet",
                    postcode = "78701"
                )

        db.session.add(new_dog)
        db.session.commit()

        result = db.session.query(IndividualDog).filter_by(nickname = "Nina", breed = 'Great Pyrenees').one()
        self.assertEqual(result.breed, 'Great Pyrenees')
        self.assertEqual(result.nickname, 'Nina')
        self.assertEqual(result.mixed, 'True')

        serialized = {
            'nickname' : "Nina",
            'breed' : "Great Pyrenees",
            'mixed' : "True",
            'species' : "Dog",
            'status' : "Adoptable",
            'gender' : "Female",
            'organization' : "Adopt A Pet",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualDog).filter_by(nickname = "Nina", breed = 'Great Pyrenees').delete()
        db.session.commit()

    def test_individual_cat_1(self):
        new_cat = IndividualCat(
                    nickname = "Software Engineer",
                    cat_breed = db.session.query(CatBreed).filter_by(name="American Shorthair").first(),
                    mixed = "False",
                    species = "Cat",
                    status = "Not adoptable",
                    gender = "Female",
                    organization = "The Organization",
                    postcode = "78701"
                )

        db.session.add(new_cat)
        db.session.commit()

        result = db.session.query(IndividualCat).filter_by(nickname = "Software Engineer", breed = 'American Shorthair').one()
        self.assertEqual(result.breed, 'American Shorthair')
        self.assertEqual(result.status, 'Not adoptable')
        self.assertEqual(result.nickname, 'Software Engineer')

        serialized = {
            'nickname' : "Software Engineer",
            'breed' : "American Shorthair",
            'mixed' : "False",
            'species' : "Cat",
            'status' : "Not adoptable",
            'gender' : "Female",
            'organization' : "The Organization",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualCat).filter_by(nickname = "Software Engineer", breed = 'American Shorthair').delete()
        db.session.commit()

    def test_individual_cat_2(self):
        new_cat = IndividualCat(
                    nickname = "CS 373",
                    cat_breed = db.session.query(CatBreed).filter_by(name="American Curl").first(),
                    mixed = "True",
                    species = "Cat",
                    status = "Adoptable",
                    gender = "Male",
                    organization = "Pet Organization",
                    postcode = "78701"
                )

        db.session.add(new_cat)
        db.session.commit()

        result = db.session.query(IndividualCat).filter_by(nickname = "CS 373", breed = 'American Curl').one()
        self.assertEqual(result.breed, 'American Curl')
        self.assertEqual(result.status, 'Adoptable')
        self.assertEqual(result.nickname, 'CS 373')

        serialized = {
            'nickname' : "CS 373",
            'breed' : "American Curl",
            'mixed' : "True",
            'species' : "Cat",
            'status' : "Adoptable",
            'gender' : "Male",
            'organization' : "Pet Organization",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualCat).filter_by(nickname = "CS 373", breed = 'American Curl').delete()
        db.session.commit()

    def test_individual_cat_3(self):
        new_cat = IndividualCat(
                    nickname = "SWE",
                    cat_breed = db.session.query(CatBreed).filter_by(name="Abyssinian").first(),
                    mixed = "False",
                    species = "Cat",
                    status = "Adoptable",
                    gender = "Male",
                    organization = "Adopt Me",
                    postcode = "78701"
                )

        db.session.add(new_cat)
        db.session.commit()

        result = db.session.query(IndividualCat).filter_by(nickname = "SWE", breed = 'Abyssinian').one()
        self.assertEqual(result.breed, 'Abyssinian')
        self.assertEqual(result.status, 'Adoptable')
        self.assertEqual(result.nickname, 'SWE')

        serialized = {
            'nickname' : "SWE",
            'breed' : "Abyssinian",
            'mixed' : "False",
            'species' : "Cat",
            'status' : "Adoptable",
            'gender' : "Male",
            'organization' : "Adopt Me",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualCat).filter_by(nickname = "SWE", breed = 'Abyssinian').delete()
        db.session.commit()

    def test_individual_cat_4(self):
        new_cat = IndividualCat(
                    nickname = "there can only be one",
                    cat_breed = db.session.query(CatBreed).filter_by(name="Bambino").first(),
                    mixed = "False",
                    species = "Cat",
                    status = "Not adoptable",
                    gender = "Male",
                    organization = "Adopt Me",
                    postcode = "78701"
                )

        db.session.add(new_cat)
        db.session.commit()

        result = db.session.query(IndividualCat).filter_by(nickname = "there can only be one", breed = 'Bambino').one()
        self.assertEqual(result.breed, 'Bambino')
        self.assertEqual(result.status, 'Not adoptable')
        self.assertEqual(result.nickname, 'there can only be one')

        serialized = {
            'nickname' : "there can only be one",
            'breed' : "Bambino",
            'mixed' : "False",
            'species' : "Cat",
            'status' : "Not adoptable",
            'gender' : "Male",
            'organization' : "Adopt Me",
            'postcode' : "78701"
        }

        # Test for serialize method
        self.assertEqual(result.serialize, serialized)

        db.session.query(IndividualCat).filter_by(nickname = "there can only be one", breed = 'Bambino').delete()
        db.session.commit()

class WebsiteTestCases(unittest.TestCase):
    def test_home_up(self):
        response = requests.request("GET", "http://54.156.72.101:80/")
        self.assertTrue(response.ok)

    def test_dog_up(self):
        response = requests.request("GET", "http://54.156.72.101:80/dogs")
        self.assertTrue(response.ok)

    def test_cat_up(self):
        response = requests.request("GET", "http://54.156.72.101:80/cats")
        self.assertTrue(response.ok)

    def test_cat_adoption_up(self):
        response = requests.request(
            "GET", "http://54.156.72.101:80/adoption?Abyssinian")
        self.assertTrue(response.ok)

    def test_dog_adoption_up(self):
        response = requests.request(
            "GET", "http://54.156.72.101:80/adoption?Affenpinscher")
        self.assertTrue(response.ok)

    def test_about(self):
        response = requests.request("GET", "http://54.156.72.101:80/about")
        self.assertTrue(response.ok)

    def test_cat_breeds_json(self):
        response = requests.request("GET", "http://54.156.72.101:80/cat_breeds")
        self.assertTrue(response.ok)

    def test_dog_breeds_json(self):
        response = requests.request("GET", "http://54.156.72.101:80/dog_breeds")
        self.assertTrue(response.ok)

# ---------
# API tests
# ---------

URL = 'http://54.156.72.101:80'


class APITestCases(unittest.TestCase):

    # Steve (writing my name here so we can keep track)

    def test_dog_breeds_api(self):
        r = requests.get(URL + '/dog_breeds').json()['dog_breeds']
        self.assertEqual(r[0]['name'], 'Affenpinscher')
        self.assertEqual(r[1]['weight_metric'], '23 - 27')

    def test_dog_breeds_pagination_1(self):
        r = requests.get(URL + '/dog_breeds',
                         params={'page': 2}).json()['meta_data']
        self.assertEqual(r['page'], 2)
        self.assertEqual(r['total_items'], 172)
        self.assertEqual(r['total_pages'], 9)

    def test_dog_breeds_pagination_2(self):
        r = requests.get(URL + '/dog_breeds').json()['meta_data']
        self.assertEqual(r['page'], 1)
        self.assertEqual(r['total_items'], 172)
        self.assertEqual(r['total_pages'], 9)

    def test_cat_breeds_api(self):
        r = requests.get(URL + '/cat_breeds').json()['cat_breeds']
        self.assertEqual(r[0]['name'], 'Abyssinian')
        self.assertEqual(r[1]['weight_metric'], '3 - 5')

    def test_cat_breeds_pagination_1(self):
        r = requests.get(URL + '/cat_breeds',
                         params={'page': 2}).json()['meta_data']
        self.assertEqual(r['page'], 2)
        self.assertEqual(r['total_items'], 67)
        self.assertEqual(r['total_pages'], 4)

    def test_cat_breeds_pagination_2(self):
        r = requests.get(URL + '/cat_breeds').json()['meta_data']
        self.assertEqual(r['page'], 1)
        self.assertEqual(r['total_items'], 67)
        self.assertEqual(r['total_pages'], 4)


if __name__ == '__main__':
    unittest.main()
