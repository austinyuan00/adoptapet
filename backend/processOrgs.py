import json
    
# '''
# Add all needed organization ids to the set orgIds
# '''
# def getIds(fileName, orgIds):
#     with open(fileName) as jsonFile:
#         data = json.load(jsonFile)
#         for animal in data:
#             orgIds.add(animal["organization_id"])

'''
Get relevant org information from orgs.json and save it to neededOrgs.json
'''
def getNeededOrgs():
    orgInfo = {}
    with open("orgs.json") as jsonFile:
        data = json.load(jsonFile)
        for org in data:
            orgId = org["id"]
            orgName = org["name"]
            orgPostcode = org["address"]["postcode"]
            orgInfo[orgId] = (orgName, orgPostcode)
    with open('orgsById.json', 'w') as json_file:
        json.dump(orgInfo, json_file, indent = 4)

getNeededOrgs()
    


