from flask import Flask, render_template, jsonify, Response, send_from_directory
from flask_sqlalchemy import SQLAlchemy, inspect
import json
import os


app = Flask(__name__, static_folder='build')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://dyoon0807:cs373group9@cs373-database.caxhcxlfhbwn.us-east-1.rds.amazonaws.com/pet_db')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://david:testpass123!@localhost:5432/bookdb')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:hexalparky123@localhost:5432/adoptapet')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:12345@localhost:5432/pets')
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
# DATABASE MODELS

# Dog Breed Model


class DogBreed(db.Model):
    '''
    Defines the DogBreed column in the database.

    Attributes:
        'name': A string of name of the breed.
        'temperament': A string of the temperament of the breed.
        'life_span': A string representing the life span of the breed.
        'weight_metric': A string giving the range of the weight of the breed in kg.
        'weight_imperial': A string giving the range of the weight of the breed in lbs.
        'height_metric': A string giving the range of the height of the breed in cm.
        'height_imperial': A string giving the range of the height of the breed in inches.
        'image_url': A string giving the url to an image of the breed.

    '''
    __tablename__ = 'dog_breed'

    name = db.Column(db.String, primary_key=True)
    temperament = db.Column(db.String)
    life_span = db.Column(db.String)
    weight_metric = db.Column(db.String)
    weight_imperial = db.Column(db.String)
    height_metric = db.Column(db.String)
    height_imperial = db.Column(db.String)
    pets = db.relationship('IndividualDog', backref='dog_breed')
    image_url = db.Column(db.String)

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'name': self.name,
            'temperament': self.temperament,
            'life_span': self.life_span,
            'weight_metric': self.weight_metric,
            'weight_imperial': self.weight_imperial,
            'height_metric': self.height_metric,
            'height_imperial': self.height_imperial,
            # 'pets' : [p.serialize for p in self.pets],
            'image_url': self.image_url
        }


# Cat Breed Model
class CatBreed(db.Model):
    '''
    Defines the CatBreed column in the database.

    Attributes:
        'name': A string of name of the breed.
        'temperament': A string of the temperament of the breed.
        'life_span': A string representing the life span of the breed.
        'weight_metric': A string giving the range of the weight of the breed in kg.
        'weight_imperial': A string giving the range of the weight of the breed in lbs.
        'wikipedia_url': A string giving the url to a wikipeida page on the breed.
        'image_url': A string giving the url to an image of the breed.

    '''
    __tablename__ = 'cat_breed'

    name = db.Column(db.String, primary_key=True)
    temperament = db.Column(db.String)
    life_span = db.Column(db.String)
    weight_metric = db.Column(db.String)
    weight_imperial = db.Column(db.String)
    wikipedia_url = db.Column(db.String)
    pets = db.relationship('IndividualCat', backref='cat_breed')
    image_url = db.Column(db.String)

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'name': self.name,
            'temperament': self.temperament,
            'life_span': self.life_span,
            'weight_metric': self.weight_metric,
            'weight_imperial': self.weight_imperial,
            'wikipedia_url': self.wikipedia_url,
            # 'pets' : [p.serialize for p in self.pets],
            'image_url': self.image_url
        }


# Individual Dog Model
class IndividualDog(db.Model):
    '''
    Defines the IndividualDog column in the database.

    Attributes:
        'nickname' : A string of the nickname of the animal.
        'breed' : A string of the breed of the animal.
        'mixed' : A string indicating whether the animal is mixed or not through either "True" or "False".
        'species'  : A string indicating the species of the animal.
        'status' : A string indicating whether the animal is adoptable or not.
        'gender' : A string indicating the gender of the animal.
        'organization' : A string of the name of the organization at which the animal is held.
        'postcode' : A string of the postcode of the organization at which the animal is held.

    '''
    __tablename__ = 'individual_dog'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nickname = db.Column(db.String)
    breed = db.Column(db.String, db.ForeignKey('dog_breed.name'))
    mixed = db.Column(db.String)
    species = db.Column(db.String)
    status = db.Column(db.String)
    gender = db.Column(db.String)
    organization = db.Column(db.String)
    postcode = db.Column(db.String)

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'nickname' : self.nickname,
           'breed' : self.breed,
           'mixed' : self.mixed,
           'species'  : self.species,
           'status' : self.status,
           'gender' : self.gender,
           'organization' : self.organization,
           'postcode' : self.postcode,
       }


# Individual Cat Model
class IndividualCat(db.Model):
    '''
    Defines the IndividualCat column in the database.

    Attributes:
        'nickname' : A string of the nickname of the animal.
        'breed' : A string of the breed of the animal.
        'mixed' : A string indicating whether the animal is mixed or not through either "True" or "False".
        'species'  : A string indicating the species of the animal.
        'status' : A string indicating whether the animal is adoptable or not.
        'gender' : A string indicating the gender of the animal.
        'organization' : A string of the name of the organization at which the animal is held.
        'postcode' : A string of the postcode of the organization at which the animal is held.

    '''
    __tablename__ = 'individual_cat'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nickname = db.Column(db.String)
    breed = db.Column(db.String, db.ForeignKey('cat_breed.name'))
    mixed = db.Column(db.String)
    species = db.Column(db.String)
    status = db.Column(db.String)
    gender = db.Column(db.String)
    organization = db.Column(db.String)
    postcode = db.Column(db.String)

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'nickname' : self.nickname,
           'breed' : self.breed,
           'mixed' : self.mixed,
           'species'  : self.species,
           'status' : self.status,
           'gender' : self.gender,
           'organization' : self.organization,
           'postcode' : self.postcode
       }


# if __name__ == "__main__":
#     db.drop_all()
#     db.create_all()

