import json
import requests

'''
Scrapes the catAPI and saves it to json file.
'''
def scrape(auth):
    headers = {
        'Authorization': auth,
    }

    params = ()

    # list of all animal dicts that will be scraped
    catBreeds = []
    response = requests.get('https://api.thecatapi.com/v1/breeds', headers=headers, params=params)
    catBreeds = response.json()

    with open('catBreeds.json', 'w') as json_file:
        json.dump(catBreeds, json_file, indent = 4)


# Hardcoded auth token:
auth = 'cdce912a-1a31-4b77-9c81-178919fbc3cb'

scrape(auth)
