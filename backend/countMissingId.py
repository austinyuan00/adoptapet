import json

def countMissing(animalFile, missingAnimals):
    with open(animalFile) as jsonAnimals:
        animalData = json.load(jsonAnimals)
        with open("orgsById.json") as jsonIds:
            idData = json.load(jsonIds)
            for animal in animalData:
                curId = animal["organization_id"]
                if curId not in idData:
                    missingAnimals.append(animal["id"])

missingAnimals = []
countMissing("dogs.json", missingAnimals)
countMissing("cats.json", missingAnimals)

with open('animalsWithMissingOrg.json', 'w') as json_file:
    json.dump(missingAnimals, json_file, indent = 4)


