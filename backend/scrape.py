import json
import requests
import time

'''
Scrapes all available pages for given animalType and saves it to json file.
'''
def scrape(animalType, auth):
    headers = {
        'Authorization': auth,
    }

    params = (
        ('type', animalType),
        ('page', '1'),
        ('limit', '100'),
    )

    # list of all animal dicts that will be scraped
    animals = []
    response = requests.get('https://api.petfinder.com/v2/animals', headers=headers, params=params)
    curDict = response.json()
    for animal in curDict['animals']:
        animals.append(animal)
    numPages = curDict['pagination']['total_pages']
    curPage = 2
    while curPage <= numPages:
        if curPage % 50 == 0:
            time.sleep(1)
        params = (
            ('type', animalType),
            ('page', str(curPage)),
            ('limit', '100'),
        )
        response = requests.get('https://api.petfinder.com/v2/animals', headers=headers, params=params)
        curDict = response.json()
        for animal in curDict['animals']:
            animals.append(animal)
        curPage += 1

    with open('cats.json', 'w') as json_file:
        json.dump(animals, json_file, indent = 4)


# Hardcoded auth token:
auth = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJ3MmpqelhoamEzT0x6akZ6U2lOZlh1YUxhMkVxMzhyclg0cTR0NGhaWlFkeWk2SlVnNyIsImp0aSI6IjFkNmViOTczZDk4MzBhOGYzNTUyMGI1ZDJkMmMxNzM2MzM3Y2E1YjVjM2Y2YzQ0ZGYzZmUzYzAxZmY5NWUzMDJmMmYyNWUwZDVjMzM0MmZmIiwiaWF0IjoxNTkzODI1MzI1LCJuYmYiOjE1OTM4MjUzMjUsImV4cCI6MTU5MzgyODkyNSwic3ViIjoiIiwic2NvcGVzIjpbXX0.uZJ2sxo76dWckrA_LPL_MUlCq5qfhqgFCdipUdHEABP9ojA2PZ8dxsJ6wO5-_PHSf5ELZ5dSx0qn6O184N4jMuG290P3Z1ZDwkoSaAkRCDGJBsSu6LPPXCS6Lod64AEJbvC-1b26REr-gQuYgJuFw9-p2sYltvZ9KZbRH5o3hP__9ds1bbGk18tLKT6Naema4H_N7VYJZ5okF4cJVl4cEmuUMrw4hlZz7HqjunTicIkLfQlIiBDoK4LDaOcmF8ghY5Vc4-suxf8SZPqPcQ5DQ612__tqjWJxESYPFhj1dWkUX0UnJlNA0-SQsxphRnmdl6uBW7bzS6oOArxcwGOFGw'

scrape('cat', auth)


