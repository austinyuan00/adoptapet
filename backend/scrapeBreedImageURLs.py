import json
import requests

'''
Scrapes the cat and dog apis for image urls, saving to separate json files
'''
def scrapeCat(auth):
    headers = {
        'Authorization': auth
    }

    params = {
    'limit' : 100
    }
    finalResult = set()
    # list of all animal dicts that will be scraped
    while True:
        initialLen = len(finalResult)
        catBreedURLs = []
        response = requests.get('https://api.thecatapi.com/v1/images/search', headers=headers, params=params)
        catBreedURLs = response.json()
        for x in catBreedURLs:
            if x['breeds'] != []:
                breedName = x['breeds'][0]['name']
                url = x['url']
                pair = (breedName, url)
                finalResult.add(pair)
        if initialLen == len(finalResult):
            break
    dict = {}
    for tup in finalResult:
        dict.update({tup[0]:tup[1]})
    with open('catBreedImageURLs.json', 'w') as json_file:
        json.dump(dict, json_file, indent = 4)

def scrapeDog(auth):
    headers = {
        'Authorization': auth
    }

    params = {
    'limit' : 100
    }
    finalResult = set()
    # list of all animal dicts that will be scraped
    strikes = 0
    while strikes < 50:
        initialLen = len(finalResult)
        dogBreedURLs = []
        response = requests.get('https://api.thedogapi.com/v1/images/search', headers=headers, params=params)
        dogBreedURLs = response.json()
        for x in dogBreedURLs:
            if x['breeds'] != []:
                breedName = x['breeds'][0]['name']
                url = x['url']
                pair = (breedName, url)
                finalResult.add(pair)
        if initialLen == len(finalResult):
            strikes += 1
        dict = {}
        for tup in finalResult:
            dict.update({tup[0]:tup[1]})
    with open('dogBreedImageURLs.json', 'w') as json_file:
        json.dump(dict, json_file, indent = 4)

# Hardcoded auth token:
catAuth = 'cdce912a-1a31-4b77-9c81-178919fbc3cb'
dogAuth = '000501a7-08d6-4099-903d-fcd94c101c08'

# scrapeCat(catAuth)
scrapeDog(dogAuth)
