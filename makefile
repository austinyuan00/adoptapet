build:
	cd frontend && yarn install && yarn build
	rm -rf backend/build
	mv frontend/build backend
test:
	python3 backend/tests.py
log:
	git log > IDB3.log
